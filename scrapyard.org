
* preamble for JACS
#+LATEX_HEADER: \documentclass[journal = jacsat, manuscript = article, layout = twocolumn]{achemso}
#+LATEX_HEADER: \setkeys{acs}{articletitle = true}
#+LATEX_HEADER: \author{Samuel D. Lotz}
#+LATEX_HEADER: \email{lotzsamu@msu.edu}
#+LATEX_HEADER: \author{Alex Dickson}
#+LATEX_HEADER: \email{alexrd@msu.edu}
#+LATEX_HEADER: \affiliation[Michigan State University]{Michigan State University, East Lansing, MI, USA}
#+LATEX_HEADER: \title{Unbiased Molecular Dynamics of 11 min Timescale Drug Unbinding Reveals Transition State Stabilizing Interactions}

** removed stuff about bottlenecks
*** methods
The bottleneck is removed from the network by setting the reactive flux to zero, and the next highest flux pathway is computed.
This procedure is continued until all but \(1 - 1 * 10^-10\) of the reactive flux is accounted for.
This procedure is implemented in the =paths= function in \gls{msmbuilder}, except for the identification of the actual edge that is the bottleneck.

*** results

Alternative unbinding pathways are nevertheless important to understand and consider.
Fig. [[fig:path-bottlenecks]](B) shows that each individual bottleneck's contribution to the total reactive flux is small.
The bottleneck of the highest flux pathway explains only \SI{0.8624}{\percent} of the flux through the network, the next highest being \SI{0.5934}{\percent} and a sum of \num{367} such bottlenecks is needed to explain \SI{80}{\percent} of the total flux through the network (see Fig. [[fig:path-bottlenecks]](C)). 
All of the reactive flux can be described by 5?? bottlenecks, and after these are removed there is no path connecting the bound and unbound ensembles.
Therefore, while the highest flux bottleneck is approximately \num{4} times larger than the mean it is within the same order of magnitude and contributes a relatively small proportion of total flux.

#+CAPTION: A) The highest flux pathway through the network is shown in red and the ensemble of paths sharing the same bottleneck (bottleneck ensemble) is shown in blue. B) The percent contribution to total forward probability flux through the network for each bottleneck is shown in descending order. C) Cumulative percent contribution of flux for the bottlenecks.
#+NAME: fig:path-bottlenecks


#+CAPTION: The solvent accessible surface area (SASA) of cluster representatives in the R_1 community plotted over the committor probabilities of the cluster nodes in the conformation state network (CSN). R_2 is not shown because the cluster nodes in the CSN were not assigned committor probabilities due to their only being transitions to the community and none returning. This indicates that R_1 and R_2 were separate binding reversal steps, and that R_1 had at least a single return to non-R_1 communities, while R_2 did not. The SASA values for R_2 (\SIlist{237.48; 260.86; 274.53; 242.94; 222.61; 240.00; 274.98}{\angstrom^2}) fall within the range of R_1 SASA values with less variation. SASA values observed are intermediate compared to the range of SASA values in the whole network (range: \SIrange{524.67}{68.691}{\angstrom^2}).
#+NAME: fig:reverse-sasa
     [[./figures/R1_SASA_committor.pdf]]

*** discussion

**** inhibitor series figure
# L
#+CAPTION: Plot of unbinding rates (\(\text{ln}~k_{\text{off}}\) (10^{-4} s^{-1})) vs. the inhibition constants (\(\text{ln}~K_{i}\) (nM)) of inhibitor ligands from Lee \textit{et al.} cite:Lee2014EpoxideHydrolaseResidenceTime that have the same scaffold (i.e. same non-R2 structure) but different R2 substituents. The structure of the R2 substituents are pictured next to each point. The result of a linear regression is plotted in black and is shown to have a slope of \num{0.472}. For this series the relationship between k_{\text{off}} and K_{i} is linear (\(R^2 = \num{0.998}\)), at least for this small sample size. When considering all inhibitors of sEH measured in cite:Lee2014EpoxideHydrolaseResidenceTime this particular series is a much better fit. \(R^2 = \num{0.999}\) for the untransformed data shown in this figure, compared to an \(R^2 = \num{0.44021}\) when considering all measured inhibitors in the same region of values for \(k_{\text{off}}\) and \(K_{i}\).
#+NAME: fig:koff-Ki
#+ATTR_LaTeX: :placement [H]
    [[./figures/koff_Ki.pdf]]

**** talking about the inhibitor series

Lee /et al./ cite:Lee2014EpoxideHydrolaseResidenceTime have suggested that this isn't the case seeing as for inhibitors with high \gls{RT} and high affinity there was a poor correlation between \glssymbol{koff} and \glssymbol{Ki} (\(R^2 = \num{0.440}\) see cite:Lee2014EpoxideHydrolaseResidenceTime SI for plot).
This sample consisted of many different modifications of both R2 and R1 groups, as well as some scaffold modifications, which shows that the \glssymbol{koff} of \gls{seh} inhibitors can be independent of bound state stabilization.
This did not however show that this is true for a particular series of optimizing modifications.
Indeed, when inhibitors which have a conserved non-R2 TPPU scaffold and only larger aliphatic R2 modifications are plotted (Fig. SL) the relationship is essentially linear (\(n = \num{3}\); \(R^2 = \num{0.999}\)).
Furthermore, the slope of the log-log plot (Fig. SL) is \num{0.472} which indicates that ratio of \gls{ts} stabilization to bound state stabilization (\(\frac{\Delta{}G_{TS}}{\Delta{}G_{BS}}\)) is low.
#+MY_TODO: citation to koff vs. Kd papers here
The equation for this *stabilization ratio (\(SR\))* is a function of the slope, \(SR = 1 - \text{slope}\), and implies that a ligand series of \(\text{slope} = \num{0.5}\) is controlling the stability of the bound state only.
 and that a slope of \num{0.0} indicates all changes to the bound state free-energy are mirrored 1-to-1 by the free energy of the transition state.
This also assumes purely competitive inhibition in order to equate the \glssymbol{Ki} to \glssymbol{Kd}.
This provides evidence towards the hypothesis that hydrophobic enlargement of the R2 group is bound state stabilizing and not destabilizing to the transition state.
Thus we question instead the role of the rest of the ligand.


The inhibitors shown in Fig. SL are among those with the highest affinities and longest \glspl{rt} identified in the study but the longest residence times by far are those which have a very bulky \iupac{hepta|fluoro|iso|propyl|phenyl} group at R1 position cite:Lee2014EpoxideHydrolaseResidenceTime.
Plots of only the R1 group (Fig. SM) and only the central scaffold (not-R2 and not-R1; Fig. SN) both show a similar (albeit noisier) trend as the non-R2 (Fig. SK) and full ligand (Fig. SF) \gls{sasa} plots.
Perhaps, optimized non-R2 moieties are transition state destabilizing.
Further simulations of inhibitors with enlarged hydropbobic R2 groups and \gls{rt} optimized R1 group ligands may uncover further mechanistic details that answer that question.
Additional experimental \glspl{skr} for promising series of ligands can also identify whether or not a given modification is stabilizing to the bound state or destabilizing the transition state.
Understanding the regime of \glssymbol{koff} control is important for drug design and discovery because it allows you to deconvolute changes in affinity from changes in \gls{ts} free energy, which is an important factor for deciding which side-groups to experiment with.
For instance, given our simulation data we predict that the hydrophobicity of the R2 group drives affinity within a specfic family of scaffolds and R1 groups.
Given this, the degrees of freedom on the scaffold and R1 could be optimized for transition state stabilization while remaining in a fairly low free-energy basin provided by the R2 group stability.
