% Created 2017-11-29 Wed 16:07
% Intended LaTeX compiler: pdflatex
\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[margin=1in]{geometry}
\usepackage{float}
\newfloat{scheme}{h}{sch}
\floatname{scheme}{Scheme}
\usepackage[scaled]{libertine}
\usepackage{siunitx}
\usepackage{glossaries}
\usepackage{booktabs}
\DeclareSIUnit\molar{\mole\per\cubic\deci\metre}
\DeclareSIUnit\Molar{\textsc{M}}
\author{Samuel D. Lotz, Alex Dickson}
\date{\today}
\title{Supplemental Information: Unbiased Molecular Dynamics of 11 min Timescale Drug Unbinding Reveals Transition State Stabilizing Interactions}
\hypersetup{
 pdfauthor={Samuel D. Lotz, Alex Dickson},
 pdftitle={Supplemental Information: Unbiased Molecular Dynamics of 11 min Timescale Drug Unbinding Reveals Transition State Stabilizing Interactions},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 24.5.1 (Org mode 9.0.3)}, 
 pdflang={English}}
\begin{document}

\maketitle
\setcounter{figure}{0}
\renewcommand{\thefigure}{S\arabic{figure}}
\renewcommand{\thetable}{S\arabic{table}}

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{../figures/run_contributions.pdf}
\caption{\label{fig:orgb923696}
The contributions of each individual run to the Conformation State Network (CSN). Nodes (and the connected edges) are colored red for a run in which at least one frame from the run was included in the cluster.}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{../figures/community_clouds.pdf}
\caption{\label{fig:org3ec9cc8}
Density isosurfaces for selected major communities of the sEH-TPPU network, as well as the transition state ensemble (TSE). Each surface is computed using a single cluster representative for each node in an ensemble. Two density isosurfaces are shown for each: an opaque high density isosurface (\(\rho = \num{0.2}\)) and a translucent low density isosurface (\(\rho = \num{0.03}\)). The viewpoints are the same in all panels and show both the front and bottom views as before. One protein conformation from the ensemble is shown for each in white. The high density regions are the same as those shown in Fig. 3. The colors are roughly the same as those in other figures for ensembles as in the main text.}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{../figures/reverse_comms_anatomy.pdf}
\caption{\label{fig:orgff62dfb}
Example ligand poses for the two reversed orientation communities R\(_{\text{1}}\) (top) and R\(_{\text{2}}\) (bottom). The protein is shown in right from the front. Tyr153 (blue), Val268 (purple), and Asp105 (orange) are shown for reference.}
\end{figure}


\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{../figures/BS_sasa.pdf}
\caption{\label{fig:org7bd28e5}
Plot of the binding site solvent accessible surface area (SASA) (\si{\angstrom^2}) \cite{Shrake1973SASA} for every cluster representative in the network over their committor probabilities. The size of the points is equal to the negative of the free energy (\(-\text{ln}(p)\)) of the cluster translated to the positive domain. The binding site was defined to be all protein atoms which were within \SI{4}{\angstrom} from any ligand atom in the PDB 4OD0 crystal structure. The Shrake-Rupley method was used for calculating SASA. Clusters which were not assigned a committor probability are not included.}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{../figures/inx_distribution_clouds_SI.pdf}
\caption{\label{fig:org03dcb4a}
Density isosurfaces and network distribution of two additional interaction classes: Gln154-NE2:Lig-O2 (top) and Phe267-O:Lig-N2 (bottom). All other details are the same as in Fig. 5 in the main text.}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{../figures/path.pdf}
\caption{\label{fig:org544bb9a}
The highest flux pathway through the network is shown in red.}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=6in]{../figures/path_bottlenecks.pdf}
\caption{\label{fig:org8170222}
The top panel shows the CSN with the highest flux pathway colored in red and the bottleneck ensemble of the highest flux path in blue. In the lower panel A) shows the percent forward flux through the CSN accounted for over the inclusion of more bottlenecks, ordered from highest to lowest flux and shows the cumulative percent flux over the bottlenecks.}
\end{figure}


\begin{figure}[H]
\centering
\includegraphics[width=7in]{../figures/exits_by_run.pdf}
\caption{\label{fig:org5cb44b2}
Poses of ligands at the exit points colored by run. Ligands are shown relative to the crystal structure in white. This shows a wide variation in geometric positions of the ligands within and among different runs.}
\end{figure}


\begin{figure}[H]
\centering
\includegraphics[width=6in]{../figures/weight_hist.pdf}
\caption{\label{fig:org1fa1563}
Frequency histogram of the exit points binned over their free energies \(-\text{ln}(p)\).}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{../figures/koff_Ki.pdf}
\caption{\label{fig:org0c03768}
Plot of unbinding rates (\(\text{ln}~k_{\text{off}}\) (10\(^{\text{-4}}\) s\(^{\text{-1}}\))) vs. the inhibition constants (\(\text{ln}~K_{i}\) (nM)) of inhibitor ligands from Lee \textit{et al.} \cite{Lee2014EpoxideHydrolaseResidenceTime} that have the same scaffold (i.e. same non-R2 structure) but different R2 substituents. The structure of the R2 substituents are pictured next to each point. The result of a linear regression is plotted in black and is shown to have a slope of \num{0.472}. For this series the relationship between k\(_{\text{off}}\) and K\(_{\text{i}}\) is linear (\(R^2 = \num{0.998}\)), at least for this small sample size. When considering all inhibitors of sEH measured in \cite{Lee2014EpoxideHydrolaseResidenceTime} this particular series is a much better fit. \(R^2 = \num{0.999}\) for the untransformed data shown in this figure, compared to an \(R^2 = \num{0.44021}\) when considering all measured inhibitors in the same region of values for \(k_{\text{off}}\) and \(K_{i}\).}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{../figures/TPPU_sasa.pdf}
\caption{\label{fig:orgd268682}
Plot of the ligand TPPU solvent accessible surface area (SASA) (\si{\angstrom^2}) \cite{Shrake1973SASA} for every cluster representative in the network over their committor probabilities. The size of the points is equal to the negative of the free energy (\(-\text{ln}(p)\)) of the cluster translated to the positive domain. Clusters which were not assigned a committor probability are not included.}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{../figures/lip_distance.pdf}
\caption{\label{fig:org0a2c39d}
Plot of the distance between an atom from the center of both the top lip of the sEH binding site (Phe151-C\(\alpha\)) and the bottom lip (Phe267-C\(\alpha\)) for every cluster representative in the network over their committor probabilities. The size of the points is equal to the negative of the free energy (\(-\text{ln}(p)\)) of the cluster translated to the positive domain. Clusters which were not assigned a committor probability are not included.}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{../figures/TPPU_R2_sasa.pdf}
\caption{\label{fig:org0b924da}
Plot of solvent accessible surface area (SASA) (\si{\angstrom^2}) \cite{Shrake1973SASA} for the atoms in the R2 substituent of the TPPU ligand, for every cluster representative in the network over their committor probabilities. The size of the points is equal to the negative of the free energy (\(-\text{ln}(p)\)) of the cluster translated to the positive domain. Clusters which were not assigned a committor probability are not included.}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{../figures/TPPU_not_R2_sasa.pdf}
\caption{\label{fig:org27c9d46}
Plot of solvent accessible surface area (SASA) (\si{\angstrom^2}) \cite{Shrake1973SASA} for all of the atom not in the R2 substituent of the TPPU ligand, for every cluster representative in the network over their committor probabilities. The size of the points is equal to the negative of the free energy (\(-\text{ln}(p)\)) of the cluster translated to the positive domain. Clusters which were not assigned a committor probability are not included.}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[height=8in]{../figures/TPPU_sasa_network.pdf}
\caption{\label{fig:org56ee270}
Conformation Space Network (CSN) colored according to the solvent accessible surface area (SASA) (\si{\angstrom^2}) of only the R2 group of the TPPU ligand (top) and only the non-R2 atoms of the TPPU ligand (bottom).}
\end{figure}


\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{../figures/P1_P2.pdf}
\caption{\label{fig:org3321c07}
Scatter plot showing the frequency of the 13 intersecting interactions between P\(_{\text{1}}\) and P\(_{\text{2}}\) community ensembles. Each point represents a specific acceptor-donor hydrogen bond pair.}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{../figures/TPPU_R1_sasa.pdf}
\caption{\label{fig:org17fa4e0}
Plot of solvent accessible surface area (SASA) (\si{\angstrom^2}) \cite{Shrake1973SASA} for all of the atoms in the R1 substituent of the TPPU ligand, for every cluster representative in the network over their committor probabilities. The size of the points is equal to the negative of the free energy (\(-\text{ln}(p)\)) of the cluster translated to the positive domain. Clusters which were not assigned a committor probability are not included.}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{../figures/TPPU_scaffold_sasa.pdf}
\caption{\label{fig:org8ab0b70}
Plot of solvent accessible surface area (SASA) (\si{\angstrom^2}) \cite{Shrake1973SASA} for all of the atom not in the R2 substituent nor in the R1 substituent (i.e. scaffold) of the TPPU ligand, for every cluster representative in the network over their committor probabilities. The size of the points is equal to the negative of the free energy (\(-\text{ln}(p)\)) of the cluster translated to the positive domain. Clusters which were not assigned a committor probability are not included.}
\end{figure}





\begin{table}[H]
\begin{tabular}{lrrrrrrrr}
\toprule
             &  count &          mean &           std &           min &          max \\
 Run Index   &        &               &               &               &              \\
\midrule
1            &    3 &  4.014382e-13 &  2.818670e-13 &  2.387022e-13 &  7.269102e-13 \\
3            &   20 &  1.937678e-10 &  2.180761e-10 &  7.852805e-12 &  7.538693e-10 \\
4            &   22 &  3.387088e-12 &  4.199034e-12 &  5.468285e-13 &  1.749851e-11 \\
5            &   27 &  4.381838e-12 &  6.320464e-12 &  3.645440e-13 &  2.333082e-11 \\
6            &    3 &  5.647383e-13 &  1.362082e-13 &  4.458460e-13 &  7.133536e-13 \\
\bottomrule
\end{tabular}
\caption{Table of statistics of exit point weights in each run they were observed.}
\label{tab:exit-point_stats}
\end{table}



\newpage
\bibliographystyle{unsrt}
\bibliography{paper}
\end{document}